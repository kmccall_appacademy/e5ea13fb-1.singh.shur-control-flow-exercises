# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.reject { |e| e.downcase == e  }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  up_mid = str.length / 2
  str.length.odd? ? str[up_mid] : str[(up_mid - 1)..up_mid]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.inject(0) { |a, v| VOWELS.include?(v) ? a + 1 : a }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  num.times { |i| product *= i + 1 }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ''
  arr[0...-1].each { |i| string << i << separator }
  string << arr[-1] unless arr[-1].nil?
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index { |e, i| (i + 1).even? ? e.upcase : e.downcase }.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map { |s| s.length >= 5 ? s.reverse : s }.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |i|
    if (i % 15).zero?
      arr << 'fizzbuzz'
    elsif (i % 3).zero?
      arr << 'fizz'
    elsif (i % 5).zero?
      arr << 'buzz'
    else
      arr << i
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  return_arr = []
  while arr.length > 0
    return_arr << arr.pop
  end
  return_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return true if num == 2
  return false if num.even? || num == 1
  mid =
        if (num / 2).even?
          (num / 2) + 1
        else
          num / 2
        end
  mid.step(3, -2) do |i|
    return false if (num % i).zero?
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = [num]
  (num / 2).step(1, -1) do |i|
    arr << i if (num % i).zero?
  end
  arr.reverse!
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |i| prime?(i) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  num_evens = arr[0...3].select(&:even?).length
  num_evens > 1 ? arr.select(&:odd?).first : arr.select(&:even?).first
end
